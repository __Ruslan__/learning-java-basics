package com.company;

public class TestArrays {
    public static void main(String[] args) {
        int y = 0;    // "y" изначально равна нулю

        String[] islands = new String[4]; // Создал массив типа String

        islands[0] = "Бермуды";             // поместил в индексы массива эти слова
        islands[1] = "Фиджи";               //
        islands[2] = "Азорские острова";    //
        islands[3] = "Косумель";            //

        int[] index = new int[4];   // новый массив index целочиленного типа

        index[0] = 1;
        index[1] = 3;
        index[2] = 0;
        index[3] = 2;


        int ref;                   // переменная целочисленного типа
        while (y < 4) {
            ref = index[y];        // переменной ref присваивается значение элемента массива
            System.out.print("острова = ");
            System.out.println(islands[ref]);
            y = y + 1;

        }


    }
}
