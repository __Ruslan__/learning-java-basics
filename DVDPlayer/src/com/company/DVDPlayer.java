package com.company;

public class DVDPlayer {              // класс DVDPlayer
    boolean canRecord = false;        // переменная класса DVDPlayer - ЛОЖЬ

    void recordDVD() {                            // метод класса DVDPlayer
        System.out.println("Идёт запись DVD");
    }

    void  playDVD () {
        System.out.println("DVD проигрывается");  // метод класса DVDPlayer
    }
}
