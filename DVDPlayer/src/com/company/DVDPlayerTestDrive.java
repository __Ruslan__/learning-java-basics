package com.company;

public class DVDPlayerTestDrive {                  // класс с внутри которого main он запускает программу и оновной
    public static void main(String[] args) {       // код в этом классе
        DVDPlayer d = new DVDPlayer();             // новый объект d класса DVDPlayer
        d.canRecord = true;                        // переменная canRecord класса DVDPlayer, присвоили значение ИСТИНА
        d.playDVD();                               // сработает метод класса DVDPlayer

        if (d.canRecord == true) {                 // сравнение логической переменной из класса DVDPlayer
            d.recordDVD();                         // т.к. переменная ИСТИНА, то сработает метод recordDVD из класса DVDPlayer
        }
    }
}
