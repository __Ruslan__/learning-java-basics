package com.company;

public class TapeDeck {              // класс TapeDeck
    boolean canRecord = false;       // логическая переменная класса (экземпляр класса), изначально ЛОЖЬ

    void playTape() {                // метод
        System.out.println("Плёнка проигрывается");
    }

    void recordTape() {              // метод
        System.out.println("Идёт запись на плёнку");
    }


}
