package com.company;

public class TapeDeckTestDrive {                      // Класс содержит главный класс main
    public static void main(String [] args) {         // и основная работа программы происходит в этом классе
        TapeDeck t = new TapeDeck();                  // новый объект t
        t.canRecord = true;                           // переменной canRecord из класса TapeDeck присвоили ИСТИНУ
        t.playTape();                                 // вызываем метод playTape из класса TapeDeck

        if (t.canRecord == true) {                    // так как переменной canRecord до того(в 6 строке) присвоили
            t.recordTape();                           // присвоили ИСТИНУ то условие сработает и запустится метод
        }                                             // recordTape из класса TapeDeck

    }
}
