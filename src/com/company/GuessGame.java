package com.company;

public class GuessGame {
    Player p1;      // содержит 3 переменных
    Player p2;      // экземпляра для 3 объектов Player
    Player p3;

    public void startGame() {
        p1 = new Player();      // создаём 3 объекта Player
        p2 = new Player();      // и присваиваем их 3 переменным экземпляра
        p3 = new Player();

        int guessp1 = 0;        // объявляем 3 переменные для хранения вариантов
        int guessp2 = 0;        // от каждого игрока
        int guessp3 = 0;

        boolean p1isRight = false;     // объявляем 3 переменные для хранения правильности
        boolean p2isRight = false;     // или неправильности ответов игроков
        boolean p3isRight = false;

        int targetNumber = (int) (Math.random() * 10);          // создаём число которое игроки
        System.out.println("Я загадываю число от 0 до 9...");   // должны угадать

        while (true) {
            System.out.println("Число, которое нужно угадать, - " + targetNumber);

            p1.guess();   // Вызываем метод guess()
            p2.guess();   // из каждого объекта Player
            p3.guess();

            guessp1 = p1.number;
            System.out.println("Первый игрок думает, что это " + guessp1);  // Извлекаем варианты каждого игрока

            guessp2 = p2.number;
            System.out.println("Второй игрок думает, что это " + guessp2);  // результаты работы из метода guess(),

            guessp3 = p3.number;
            System.out.println("Третий игрок думает, что это " + guessp3);  // получая доступ к их переменным number

            if (guessp1 == targetNumber) {   // сверяем варианты каждого из игроков
                p1isRight = true;            // на соответствие загаданному числу.
            }
            if (guessp2 == targetNumber) {   // Если игрок угадал то присваиваем соответствующей переменной
                p2isRight = true;            // значение true (переменная по умолчанию хранит значение false)
            }
            if (guessp3 == targetNumber) {
                p3isRight = true;
            }

            if (p1isRight || p2isRight || p3isRight) {
                System.out.println("У нас есть победитель!");
                System.out.println("Первый игрок угадал?" + p1isRight);
                System.out.println("Второй игрок угадал?" + p2isRight);
                System.out.println("Третий игрок угадал?" + p3isRight);
                System.out.println("Конец игры.");
                break;  // игра окончена, прерываем цикл
            } else {
                System.out.println("Игроки должны попробовать ещё раз.");
            }

        }
    }
}
