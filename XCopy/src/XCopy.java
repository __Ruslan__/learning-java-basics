public class XCopy {
    public static void main (String[] args){
        int orig = 42;

        XCopy x = new XCopy();  // новый объект

        int y = x.go(orig);     // переменная y передаёт в метод go значение 42 в arg,
                                // arg умножается на 2, поэтому y=84 (или проще, передаётся аргумент orig в параметр arg)

        System.out.println(orig + " " + y);  // переменная orig не меняется, поэтому вывод 42 84
    }
    int go(int arg){
        arg = arg * 2;
        return arg;
    }
}
