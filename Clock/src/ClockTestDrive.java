public class ClockTestDrive {
    public static void main (String[] args){   // основной класс
        Clock c = new Clock();                 // новый объект с
        c.setTime("1245");                     // вызываем метод setTime и устанавливаем значение 1245 в time
        String tod = c.getTime();              // возвращаем значение time в переменную tod
        System.out.println ("время: " + tod);  // пишем/выводим текст: время: 1245
    }
}
